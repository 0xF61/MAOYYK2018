# DNS

**Hostname:** Makinaya verilen kimlik bilgisi.

**Domain:** Bir grup bilgisayarın oluşturduğu ağa verilen kimlik bilgisi.

Her makinada nameserver, dhcp veya elle atatanmış olduğu için oradaki domain
sunucularına sorar (Recursive).  TLD(TopLevelDomain)'e sorar o sunucuda .tr
kaydı tutan sunuculara sordurur burdan org.tr domainlerini tutan sunucuya
yönlendirir burdan da linux.org.tr sunucusunun ip adresini cachler ve bundan
sonraki isteklerde bu yapılan işlem tekrardan gerçekleşmez.

DNS(Domain Name System) website adreslerini ip adreslerine çevirmek için
kurulmuştur. 2003 yıllından sonra websitelerinin sayısı ip adreslerini geçmeye
başlayacağı için tek bir makinada birden fazla site bulunmaya başlamıştır.

İşletim sistemi ilk olarak gelen DNS kaydını /etc/hosts veya
/system32/drivers/etc/hosts dosyasına bakar varsa zaten direk ip adresini
kullanır. Eğer herhangi bir kayıt yoksa, DHCP serverin bize atadığı nameserver
olan 1.1.1.1 olan sunucuya dns protokolü üzerinden "linux.org.tr" domaninini
sorar eğer onun üzerinde de bu kayıt yoksa. 1.1.1.1'de ki sunucu kaydı bilmese
de *.tr kayıtlarını tutan nic.tr sunucusuna "linux.org.tr" sorar. En son
linux.org.tr'nin geriye doğru sunuculara ip adresi eklenir ve 1.1.1.1 bize
istediğimiz domainin ip adresini döndürür ve bizden biraz sonra 1.1.1.1'e
linux.org.tr'yi sorarsa bu işlemler tekrardan yaşanmaz. Artık 1.1.1.1'in kendi
içinde "linux.org.tr" kaydı olduğu için daha sonraki dns isteklerine cevap
verebilir hale gelmiştir.

linux.org.tr içinde mail ve web siteleri olabileceği için DNS kayıt tipleri
vardır:

A:  Website kayıtları için.

MX: Mail atmak istenildiğinde sorulan DNS kaydıdır.

TXT: Text, validation için kullanılabilir.

CNAME: Yönlendirme yapmak istenildiği zaman kullanılır mesela
admin.linux.org.tr domainini yonetim.linux.org.tr'ye linkleyebiliriz.

AAA: IPv6 kayıtları için.

```
$ nslookup
> server
Default server: X.X.X.X
Address: X.X.X.X#53
$ dig linux.org.tr
...
linux.org.tr.       2846    IN  A   139.179.179.3
...
$ dig MX linux.org.tr # MAIL Server
linux.org.tr.       7200    IN  MX  10 postaci.linux.org.tr.
$ nslookup postaci.linux.org.tr
...
Server:     192.168.42.129
Address:    192.168.42.129#53
...
$ dig NS linux.org.tr # DNS Serverleri
...
linux.org.tr.       7199    IN  NS  marmara.linux.org.tr.
linux.org.tr.       7199    IN  NS  trakya.linux.org.tr.
...
```

```
$ dig . ns # TopLevelDomain listesi
...
.                       94359   IN      NS      c.root-servers.net.
.                       94359   IN      NS      f.root-servers.net.
.                       94359   IN      NS      j.root-servers.net.
.                       94359   IN      NS      d.root-servers.net.
.                       94359   IN      NS      e.root-servers.net.
.                       94359   IN      NS      a.root-servers.net.
.                       94359   IN      NS      l.root-servers.net.
.                       94359   IN      NS      h.root-servers.net.
.                       94359   IN      NS      k.root-servers.net.
.                       94359   IN      NS      m.root-servers.net.
.                       94359   IN      NS      g.root-servers.net.
.                       94359   IN      NS      i.root-servers.net.
.                       94359   IN      NS      b.root-servers.net.
...
$ dig www.linux.org.tr a @a.root-servers.net
...
;; AUTHORITY SECTION:
tr.                     172800  IN      NS      ns21.nic.tr.
tr.                     172800  IN      NS      ns22.nic.tr.
tr.                     172800  IN      NS      ns31.nic.tr.
tr.                     172800  IN      NS      ns41.nic.tr.
tr.                     172800  IN      NS      ns42.nic.tr.
tr.                     172800  IN      NS      ns91.nic.tr.
tr.                     172800  IN      NS      ns92.nic.tr.
...
$ dig www.linux.org.tr a @ns21.nic.tr.
; <<>> DiG 9.10.3-P4-Debian <<>> www.linux.org.tr a @ns21.nic.tr.
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 32917
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 2, ADDITIONAL: 3
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;www.linux.org.tr.              IN      A

;; AUTHORITY SECTION:
linux.org.tr.           43200   IN      NS      marmara.linux.org.tr.
linux.org.tr.           43200   IN      NS      trakya.linux.org.tr.

$ dig www.linux.org.tr a @marmara.linux.org.tr

Bu sunucular recursive olmayan isteklere yanıt vermez.
'''

---

'''

$ dig www.tokgozoglu.net @52.178.110.255

; <<>> DiG 9.10.3-P4-Debian <<>> www.tokgozoglu.net @52.178.110.255
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 14270
;; flags: qr aa rd; QUERY: 1, ANSWER: 2, AUTHORITY: 2, ADDITIONAL: 3
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;www.tokgozoglu.net.            IN      A

;; ANSWER SECTION:
www.tokgozoglu.net.     86400   IN      CNAME   tokgozoglu.net.
tokgozoglu.net.         86400   IN      A       52.178.110.255

;; AUTHORITY SECTION:
tokgozoglu.net.         86400   IN      NS      ns2.tokgozoglu.net.
tokgozoglu.net.         86400   IN      NS      ns1.tokgozoglu.net.

;; ADDITIONAL SECTION:
ns1.tokgozoglu.net.     86400   IN      A       52.178.110.255
ns2.tokgozoglu.net.     86400   IN      A       52.178.110.255

;; Query time: 8 msec
;; SERVER: 52.178.110.255#53(52.178.110.255)
;; WHEN: Mon Jul 23 11:22:12 +03 2018
;; MSG SIZE  rcvd: 145

$ dig www.tokgozoglu.net @8.8.8.8

; <<>> DiG 9.10.3-P4-Debian <<>> www.tokgozoglu.net @8.8.8.8
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 15285
;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;www.tokgozoglu.net.            IN      A

;; ANSWER SECTION:
www.tokgozoglu.net.     21599   IN      CNAME   tokgozoglu.net.
tokgozoglu.net.         21599   IN      A       188.166.45.9

```

**[!] İlk sorguda 86400 iken google sunucusunda 21599 olmasının nedeni
google'ın dns sunucusunun cacheinde bu domainin kaydının olmasıdır.**

## Scapy

```
$ scapy
>>> ip = IP(dst="192.168.42.129",version=4)
>>> ud = UDP(sport=61,dport=53)
>>> dn = DNS(rd=1,qd=DNSQR(qname="linux.org.tr"))
>>> send(ip/ud/dn)
```

**[!] Wireshark üzerinden dönen dns sorgusu incelenebilir.**

```
$ hping3 # Paket üretmek için kullanılabilir
$ hping3 --udp --destport 53 --rand-source <NSServer> --flood
```

