# OSI Katmanları 

| * | Katmanlar       |
| - | :-------------: |
| 1 | Fiziksel        |
| 2 | Data            |
| 3 | Network         |
| 4 | Taşıma          |
| 5 | Oturum          |
| 6 | Sunum           |
| 7 | Uygulama        |

**[!] Yukarıdaki katmanların amacı sistemdeki her katmanın birbirlerinden
etkilenmeden kendi içlerinde değişiklik uygulanılabilmesidir.**

**1) Fiziksel Katman** : BitStream(0-1). Fiber, Bakır tel gibi fiziksel aktarım
yöntemleri kullanılan katmandır.

**2) Data Katmanı** : Bu katmanda fiziksel ağ donanımının adresi kullanılır
_MAC_ . Ethernet varsa ARP(Adress Resoluiton Protocol) örnek verilebilir. ARP ve
RARP sorgularıyla MAC veya IP adresi bilinen bir makinanın diğer bilgisi
öğrenilebilir.

MAC Adresi, Bir ağdaki cihazı tanımlanmasını sağlayan fiziksel ağ kartının
üzerindeki adresi belirtir. Bu adres 6 oktetten oluşur
(00:00:00:00:00:00-FF:FF:FF:FF:FF:FF). MAC adresinin ilk 3 okteti cihazı üreten
firmayı, son 3 oktet ise her kart için benzersizdir.

IP --ARP--> MAC

MAC --RARP--> IP

ARP tablosunda iletişim kurulmak istenen makinanın mac adresi yoksa, icmp
requesti (ping), iletişim kurulmak istenen sistemin ip adresine gönderilerek
karşılıklı olarak arp tablolarını güncelleyebiliriz.

ARP tablosunu kontrol etmek için terminalde: Linux sistemlerde "arp -an",
Windows sistemlerde "arp -a" yazılarak tablo gösterilebilir. ARP tablosundan
girdi silmek için her iki sistemde de aynı "arp -d" komutu kullanılabilir. 

MAC adresinin kullanıldığı protokollere aşağıdakiler örnek olarak verilebilir.
* Ethernet
* Token ring
* Wi-fi
* Bluetooth
* FDDI
* SCSI

**3) Network Katman** : TCP/IPv4 veya TCP/IPv6 ile adreslerin oluşturulup
makinalara tanımlandığı katman.

IPv4 32bitlik adresleme yapabilir. 2^32 den 4294967296 adres üretilebilir ama
bunu daha kolay şekilde okuyabilmek için 32 bit 4'erli gruplar halinde 0-255
arasında ifade edilir.

0.0.0.0 <--> Localhost

255.255.255.255 <--> Broadcast (Yayın Adresi)

**4) Taşıma Katman** : İletişim protokolleri TCP veya UDP kullanılır. TCP
outurum kontrolü yaparken, UDP herhangi bir iletişim veya paket bütünlüğü
kontrolü yapmaz. UDP bu yüzden TCP'den daha hızlıdır.

**5) Oturum Katman** : Servislerin birbirleriyle iletişimini sağlayan katman.
(SSH/RDP/SMB)

**6) Sunum Katman** : Paketi uygun hale getirir. Çeşitli format değişimleri
burada yapılır. (PNG--base64-->ascii, SSL,TLS)

**7) Uygulama Katman** : Firefox, FTP, SMTP, HTTP
