# TCP/IP Protokol Kümesi

**[!] OSI katmanı referans olarak alındığı için birebir olarak günümüzde
kullanılmaz.**

| Cihaz veya Uygulamlar   | Paketin İsmi      | TCP/IP          | OSI Katmanı   |
| :--------------------:  | :---------------: | :-------------: | :----------:  |
| SSL                     |                   |                 | Application   |
| VPN                     |                   |                 | ------------  |
| Application Firewall    | Data              | Applicaiton     | Presentation  |
| DLP                     |                   |                 | ------------  |
|                         |                   |                 | Session       |
| ----------------------  | ----------------- | --------------- | ------------  |
| Firewall, Load Balancer | Segment/Datagram  | Transport       | Transport     |
| ----------------------  | ----------------- | --------------- | ------------  |
| Router                  | Packet            | İnternet        | Network       |
| ----------------------  | ----------------- | --------------- | ------------- |
| Switch                  | Frame             |                 | Data Link     |
| RJ45                    | ----------------- | Network Access  | ------------  |
| Cat5                    | BitStream         |                 | Physical      |
