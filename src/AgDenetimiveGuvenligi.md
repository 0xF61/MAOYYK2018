Kamp içeriği olarak Ağ Yönetimi konusuna giriş yapıldıktan sonra varsayılan
ayarlardan kaynaklanan güvenlik zafiyetleri anlatılacak ve bununla birlikte
savunma bakış açısı güçlendirilebilecektir.

Güvenlik konusunda danışmanlar ve saldırganlar arasındaki benzerlik ve
farklılıklardan bahsederek kampa başladık. Saldırganlar motivasyon ve zaman
açısından, güvenlik danışmanlarına göre daha şanslıdırlar. Danışmanların belirli
bir çalışma aralığı olmasına rağmen saldırganların motivasyon kaynağına göre bu
zaman kavramı genişleyebilir. Ayrıca danışmanlar firma tarafında çalışan
sistemlerin kararlılığının bozulmaması amacıyla firma tarafından bazı
sistemlerin erişimine izin verilmeyebilir. Saldırganlar ise bu konuda daha çok
esnektir ve sistemin kararlılığını korumak gibi bir amaçları olmadığından ciddi
hasarlar verebilirler. Zafiyet bulunduktan sonra danışmanlar sömürüyü
gerçekleştirirken bunu ispatlamak için yaparken, saldırganlar sistemin
içerisinde istedikleri bilgiyi elde etmek için kendi amaçları doğrultusunda
kullanırlar. Saldırganların sistemden izini silmek ve daha sonra tekrardan
erişebilmek için arka kapı bırakırlar fakat güvenlik danışmanlarının böyle bir
amacı yoktur.

**Hacking:** Bir sistemi çalışması planlanan şekli dışında kullanmak.

**Ethical Hacking:** Sistemlere sızmak için saldırganlar tarafından kullanılan
yöntemlerin kurumların güvenlik seviyesini arttırmak amacıyla, siber tehditlere
ve veri sızıntılarına karşı test edilmesi ve daha etkili savunma yöntemlerinin
geliştirilmesini savunan felsefedir.

**Risk:** Tehdit ve zafiyetin kesiştiği nokta.

**Total Risk Analizi:** Sistemde ki bir zafiyetin ne kadar risk
oluşturabileceğini raporlayıp analiz etmektir.

**Zafiyet:** Bir sistemin olağan davranışını bozmasına sebep olan nokta.

**[!] Sadece zafiyet taraması veya Bug Bounty yapılarak total risk analizi
yapılamaz.**

**Sömürü (exploitation):** Zafiyeti veya zafiyetleri kullanarak sistemde yer
edinme işlemi.

**Post Explotation** Sömürü gerçekleştirildikten sonra bilgi toplama ve başka
sistemlere geçme aşamasına verilen isim.

**Payload:** Exploit ile sistemde yer edindikten sonra çalıştırılmak istenen kod
parçası.

**Penetration Test:** Hacking işleminin teknik aşamalarını kapsayan sürece
verilen genel adıdır.

**Vulnerability/Security Assessment:** Bir sistemdeki muhtemel tüm açıkların
belirlenmesine yönelik tasarlanmış bir testtir. Sızma testinin bir aşamasıdır.

**Güvenlik denetimi:** Güvenlik araştırmacıları için total riski raporlamakken
saldırganların bunu kendi lehine kullanması amacıyla yaptıkları denetimdir.

**[!] Zafiyet bulunduktan sonra güvenlik danışmanlarının zafiyeti düzeltmek gibi
bir amacı yoktur.**

**[!] Sızma testi raporlarında zafiyetten bahsedilir ama asıl amaç zafiyetlerin
raporlanıp verilmesi değil olası senaryolarla firmayı bilgilendirmektir.**

**Risk analizi disiplini:** Birimlerin ayrı ayrı güvenliğinin incelenmesi olarak
tanımlandırılabilir. Sadece kale kapısını tutmak değil içerideki kapıları da
sağlamlaştırmak buna örnek olarak verilebilir.

Bazı firmalar güvenlik açığının bildirilmesi üzerine zafiyeti düzeltmemeyi
seçebilirler çünkü zafiyeti düzeltmek firmalar için daha masraflı olabilir.
Bu durumda danışmanlar zafiyeti puanlayarak bir bilgilendirme yapar. Yani
danışmanlar her zaman zafiyeti düzeltme amacı gütmez. Zafiyetin ne kadar büyük
bir risk oluşturabileceğini raporlar.

Güvenlik denetimlerinin standartlarının olması, ihtiyaçların giderilmesi ve
güvenlik danışmanlarının kaybolmaması için metodolojilere ihtiyaç vardır.  Bazı
kurumlar kendi standartlarını yayınlar.
* [OWAP](https://www.owasp.org/): Web App Security
* [NIST](https://www.nist.gov/): National Institute of Standards and Technology.
* [OSSTMM](http://www.isecom.org/research/): Yukarıdaki firmalara göre daha kapsamlıdır.

Karşı tarafın izni olmaması durumunda yapılan herhangi bir zafiyet taraması suç
teşkil etmektedir. Tesadüfen bulunan bir zafiyet var ise bunu bildirmek herhangi
bir suç teşkil etmemektedir. Kurumlar bu konuda eskiye göre daha çok anlayışlı
davranmaktadırlar. Profesyonel olarak test yapılacağı zaman gizlilik anlaşması
yapılır. Bu anlaşmanın içinde ilgili firmadan alınan bilgilerin ne kadarının
yayınlanabileceği ve yapılacak işlemler belirtilerek sözleşme imzalanır. Daha
sonrasında bu testler gerçekleştirilerek ortaya bir rapor konur. Bu çoğunlukla
hard-copy bile olmaz hatta tek bir pdf dosyası bile olabilir.

**Sızma Testleri - Genel Prosedür:**
* Hazırlık Aşaması
    * NDA imzalanması (Gizlilik anlaşması)
    * Test içeriğinin belirlenmesi
    * Güvenlik Sözleşmesinin İmzalanması
    * Sızma Testi Takımının Belirlenmesi
* Güvenlik Testinin Gerçekleştirimesi
* Sonuçların Alınması ve Raporlanması

Sızma Testi Kapsamları:
* Public Ip Adresi Aralıkları
* Özel Uygulamalar
* Özel Alan Adları ve Subdomainler

Güvenlik Denetim Türleri:
* Network Testleri
* İstemci Testleri
* Cloud(Bulut) Testleri
* Web Uygulama Testleri
* Kablosuz Ağ Testleri
* Sosyal Mühendislik Testleri
* Mobil Uygulama Testleri
* Tersine Mühendislik
* Sosyal Mühendislik veya Servis dışı bırakma (Ddos) testleri
* Gömülü Sistem Testleri

**[!] Çok nadir olmakla birlikte fiziksel olarakta test yapılabilir. Mesela boş
bir bilgisayara erişim sağlamak, kameralara yakalanmadan yasak yerlere
girebilmek bu test için örnek verilebilir.**
