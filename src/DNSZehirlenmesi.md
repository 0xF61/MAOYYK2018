# DNS Zehirlenmesi (DNS POISONING)

DNS sunucusuna kendisinde olmayan bir kaydı sorup aynı zamanda sorusunun
cevabını da yollaması halinde DNS sunucusunun cacheine istediği kaydı
yazdırabilir.

IP adresi bilinmeyen bir siteyi recurse olarak **sorgulamazsak.**

```
$ dig 0xf61.com +norecurse
; <<>> DiG 9.10.3-P4-Debian <<>> 0xf61.com +norecurse
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: SERVFAIL, id: 13176
;; flags: qr ra; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;0xf61.com.      IN  A

;; Query time: 1 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Sun Jul 29 11:07:03 +03 2018
;; MSG SIZE  rcvd: 49
```
---

IP adresi bilinmeyen bir siteyi recurse olarak **sorgularsak.**

```

$ dig 0xf61.com

; <<>> DiG 9.10.3-P4-Debian <<>> 0xf61.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 58885
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;0xf61.com.      IN  A

;; ANSWER SECTION:
0xf61.com.   3599    IN  A   1.1.1.1

;; Query time: 103 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Sun Jul 29 11:07:07 +03 2018
;; MSG SIZE  rcvd: 65
```
---
Tekrardan recurse sorgu olmadan sorarsak.
```
$ dig 0xf61.com +norecurse

; <<>> DiG 9.10.3-P4-Debian <<>> 0xf61.com +norecurse
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 50059
;; flags: qr ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;0xf61.com.      IN  A

;; ANSWER SECTION:
0xf61.com.   3596    IN  A   1.1.1.1

;; Query time: 2 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Sun Jul 29 11:07:10 +03 2018
;; MSG SIZE  rcvd: 65

```
