# Zafiyet

Riski oluşturan erişim noktasıdır.

##  Zafiyet Türleri

* Sömürü Tipleri:
    * Remote (Uzaktan Erişim sağlanır.)
    * Local (Doğrudan Erişim gerekir.)
    * 0-Day (Sıfırıncı Gün Açığı)
* Payload
    * Reverse: Sunucu -> Client
    * Bind: Client -> Sunucu
* PostExxploitation
* Otomatize
    * [Nessus](https://www.tenable.com/downloads/nessus)
        * [Aktivasyon](https://www.tenable.com/products/nessus/activation-code)
    * [OpenVAS](https://www.kali.org/penetration-testing/openvas-vulnerability-scanning/)
    * Core Impact
* Version/Patch
    * exploit-db.com
    * cvedetails.com
    * 0day.today
    * github.com
* Nmap
    * --scripts=vulners
        * smb-vuln-ms08-67.nse
* Nikto
* Fuzzing

## Zafiyet İstismarı

* Kaynak Koddan
* Uygulama Çatıları
    * Metasploit-Framework
        * Auxiliary: Sistemi tanımaya yarayan modüller
        * Exploit: Zafiyeti sömürmek için kullanılabilecek modüller
        * Payload: Zafiyet sömürüldükten sonra çalıştırılabilecek modüller
        * Post: Sisteme erişildikten sonra yetki yükseltmek veya verilerini
          almak gibi modüller bulundurur
        * Encoders: Oluşturulan bir çalıştırılabilir dosyayı antivirüs
          programlarından kaçırmak için modüller.
        * Meterpreter: Bir çeşit kabuk(shell) denilebilir. Port yönlendirme,
          ekran görüntüsü alma ve klavye tuşlarını yakalamak gibi ekstra
          özelliklere sahiptir.
    * Msfvenom
        * -p #Payload, -f #Format
        * Encoders
            * --list-encoders
            * -e

[g0tmi1k](https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/)  :  [Usage](https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/)
[PsPy](https://github.com/DominicBreuker/pspy)
