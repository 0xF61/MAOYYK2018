# DHCP(Dynamic Host Configuration Protocol)

Dinamik olarak ağa katılan bilgisayarlara ip ataması yapan server. Broadcast
yaparken UDP kullanır (TCP'de 3way-handshake olduğu için zaten ip adresi olması
gerekir). DHCP, ip aralığı içinde olan bir adresi yeni gelen makinalara
dağıtır. DHCP kira süresi ayarlanabilir olmakla birlikte belirli süreler
içerisinde kira süresini tekrar yeniler. Kira süresi bittikten sonra
yenilenmesi durumunda aynı ip adresini vermek gibi bir zorunluluk bulunmaz.

## Scapy

DHCP isteğinde client 68. UDP portundan dinlerken DHCP server 68. UDP portundan
cevap verir.

```
$ scapy
>>> eth = Ether(dst="ff:ff:ff:ff:ff:ff",src="aa:aa:aa:aa:aa:aa")
>>> ip  = IP(src="0.0.0.0",dst="255.255.255.255")
>>> ud  = UDP(sport=68,dport=67)
>>> bootp = BOOTP(chaddr="aa:aa:aa:aa:aa:aa", xid=100)
>>> dhcp = DHCP(options = [ ("message-type","request"),("server_id","192.168.42.254"),("requested_addr","192.168.42.254"),"end"])
>>> sendp(eth/ip/ud/bootp/dhcp)

```
