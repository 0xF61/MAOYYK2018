# TCP/UDP

**TCP:** Bir makinayla iletişime geçmek istendiğinde diğer makina geri dönüş
yaparak iletişimi onaylar ve iletişim sürekli birbirlerinin paket onaylarını
bekleyerek devam eder. Bunun faydası ise herhangi bir kayıp olmadan verinin
karşı tarafa iletilmesini sağlayabilmesidir. Bağlantı kurulurken ve
koparilirken de bu üçlü el sıkışma yapılır.

**UDP:** Paket hedef makinaya gönderilir ama herhangi bir onay sistemi olmadığı
için paketin gidip gitmediği tam olarak bilinemez. Gelmeyen paketlerde uygulama
bunu kontrol etmek zorundadır. UDP'nin paket bütünlüğüyle ilgili herhangi bir
kontrol mekanizması yoktur.

## Üçlü El Sıkışma (3 Way HandShake)

-----SYN--->

<--SYN-ACK--

----ACK---->

----FIN---->

<-FIN-ACK---

----ACK---->

---

## TCP HEADER

![tcp-header](pic/tcp_header.png)

**Source Port:** Kaynak makinanın Portu

**Destination Port:** Hedef makinanın Portu

**Sequence Number:** Senkronize Numarası

**Acknowledgement Number:** En son alınan senkron paketinin bir fazlası

**Hlen:** TCP başlık kısmının uzunluğı

* Flags
    * URG:  Acil Biti
    * ACK:  Acknowledge Biti
    * PSH:  Push Biti
    * RST:  Reset Biti
    * SYN:  Senkron Biti
    * FIN:  Bitiirme Biti

**Windows:** Alıcının kaç paketi birden istediği bilgisi

**Checksum:** Paketin bütünlüğünü doğrular.

**Urgent Pointer:** URG biti işaretlendiyse kullanılır.

---

## IP HEADER

![ip-header](pic/ip_header.png)

**Version:** IPv4 veya IPv6

**Length:** Paketin uzunluğu

**Type of Service:** Servis tipini belirtir Routerlar buna göre öncelik ataması
yaparlar

**Total Length:** Paketin toplam uzunluğu

**Identifier:** Her paketin numarası

**Flag:** Paketin parçalanıp parçalanmayacağı bilgisi

**Fragmented Offset:** Parçalanmış paketlerin sıra numarası

**Time to Live:** Paketin yaşam süresi

**Protocol:** TCP ya da UDP hangi alt katmanlı paket içerdeiğini gösterir

**Header Checksum:** Doğrulamayı barındırır

**Source IP:** Kaynak ip

**Destination IP:** Hedef ip

---

## Scapy 3way-Handshake

```
$ iptables -I OUTPUT -p tcp --tcp-flags RST RST -j DROP # RST Sinyalini engellemek için
$ nc -lvp 80 # Başka bir terminalden
$ scapy
>>> conf.L3socket=L3RawSocket
>>> ip = IP(src="127.0.0.1",dst="127.0.0.2")
>>> SYN = TCP(sport=1234,dport=80,flags="S")
>>> veri = "Merhaba\n"
>>> sr1(ip/SYN)
Begin emission:
.*Finished sending 1 packets.

Received 2 packets, got 1 answers, remaining 0 packets
<IP  version=4 ihl=5 tos=0x0 len=44 id=0 flags=DF frag=0 ttl=64 proto=tcp chksum=0x3cc9 src=127.0.0.2 dst=127.0.0.1 options=[] |<TCP  sport=www_http dport=search_agent seq=2957853120 ack=1 dataofs=6 reserved=0 flags=SA window=43690 chksum=0xfe21 urgptr=0 options=[('MSS', 65495)] |>>
>>> ACK = TCP(sport=1234,dport=80,flags="A",seq=1,ack=2957853121)
>>> sr1(ip/ACK/veri)
Begin emission:
Finished sending 1 packets.
.*
Received 2 packets, got 1 answers, remaining 0 packets
<IP  version=4 ihl=5 tos=0x0 len=40 id=26505 flags=DF frag=0 ttl=64 proto=tcp chksum=0xd543 src=127.0.0.2 dst=127.0.0.1 options=[] |<TCP  sport=www_http dport=search_agent seq=2957853121 ack=9 dataofs=5 reserved=0 flags=A window=43690 chksum=0xfe1d urgptr=0 |>>
>>> FINACK = TCP(sport=1234,dport=80,flags="FA",seq=9,ack=2957853121)
>>> sr1(ip/FINACK)
Begin emission:
Finished sending 1 packets.
.*
Received 2 packets, got 1 answers, remaining 0 packets
<IP  version=4 ihl=5 tos=0x0 len=40 id=26506 flags=DF frag=0 ttl=64 proto=tcp chksum=0xd542 src=127.0.0.2 dst=127.0.0.1 options=[] |<TCP  sport=www_http dport=search_agent seq=2957853121 ack=10 dataofs=5 reserved=0 flags=FA window=43690 chksum=0xfe1d urgptr=0 |>>
>>> ACK = TCP(sport=1234,dport=80,flags="A",seq=10,ack=2957853121)
>>> sr1(ip/ACK)
Begin emission:
.Finished sending 1 packets.
*
Received 2 packets, got 1 answers, remaining 0 packets
<IP  version=4 ihl=5 tos=0x0 len=40 id=22525 flags=DF frag=0 ttl=64 proto=tcp chksum=0xe4cf src=127.0.0.2 dst=127.0.0.1 options=[] |<TCP  sport=www_http dport=search_agent seq=2957853121 ack=0 dataofs=5 reserved=0 flags=R window=0 chksum=0xbaac urgptr=0 |>>
```


![tcp-scapy-3way-handshake](pic/tcp-scapy-3way-handshake.png)

---

## PORT DURUMLARI

### TCP

| TCP/Port | Açık    | Kapalı | Cevap Gelmezse    |
| -------- | :-----: | :----: | :---------------: |
| SYN      | SYN+ACK | RST    | Firewall olabilir |
| ACK      | RST     | RST    | Makina Yok        |
| FIN      | RST     | RST    | Bağlantı Problemi |
| URG      | RST     | RST    | Servis Dışı Kalma |
| RST      | X       | X      |                   |

---

### UDP

| UDP/Port | Açık          | Kapalı                   | Cevap Gelmezse |
| -------- | :---:         | :---:                    | :----------:   |
|          | Cevap Gelirse | ICMP(Port Unreach) Döner | Açık/Firewall  |

