# Aktif Bilgi Tarama

* Host Discovery
    * netdiscover -r >ipBLOĞU<
    * arp-scan -I eth0 -l
* Network Discovery
    * nmap -sP >ipBLOĞU< #pingSweep
    * nmap -oX scan.xml # xsltproc xml -> html
* Port Scanning
    * nmap -sS #SYN, -sT #TCP, -sA #ACK, -sF #FIN, -sU #UDP
    * nmap -p-, --top-ports
    * nmap -sI # Zombie Scan
    * nmap -f # FTP Scan
    * nmap -Pn --disable-arp-ping # Hiç bir Tarama Yapmaz
* Banner Grabbing
    * nc, curl, telnet
* Version Detection
    * nmap -sV # Version Scan
    * nmap -O # OS
* Firewall Bypass
    * nmap -f -f 
    * nmap --script=firewall-bypass
    * nmap -D RND:5
    * nmap --spoof-mac=IBM >ipBLOĞU<
* Agressive Scan
    * nmap -A # Aggressive
    * nmap -T (0-5)
* DNS Discovery
    * nslookup
    * dig ns , dig -x
    * fierce --dns
    * dnsrecon -d
* Web Application Discovery
    * WP, Drupal, Joomla
    * wafw00f
* Fuzzing
    * dirb
    * dirbuster
    * wfuzz
    * nikto
* Internal
    * Internal Ranges
    * Directory Services
    * Enterprise Applications
