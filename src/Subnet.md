# Subnet (Alt Ağ)

IP Havuz aralığı içerisinde ihtiyaç kadar ip adresi tanımlamak için kullanılır.

192.168.48.130

11000000.10101000.00111000.100|00010

11111111.11111111.11111111.111|00010

255.255.255.224

11111111.11111111.11111111.111|00000 (Alt Ağ Adresi)

11111111.11111111.11111111.111|11111 (Alt Ağ Yayın Adresi)

Dünya çapıında bu ip bloklarının yönetimini IANA (Internet Assigned Numbers
Authority) yapar. Zamanla ip adresi yetmemeye başladığından NAT(Network Address
Translation) ortaya çıkmıştır. Bunun yapılabilmesi için tek gereken şey
internet ile alt ağlar arasında **adres dönüşümü** gerçekleştirilmesidir. 

10.0.0.0/8 A <--> Yaklaşık olarak 16 milyar ip adreslenebilir. [ 2^(32-8) ]

172.16.0.0./12 <--> Yaklaşık olarak 1 milyar ip adreslenebilir. [ 2^(32-12) ]

192.168.0.0/16 <--> Yaklaşık olarak 65 bin ip adreslenebilir. [ 2^(32-16) ]

127.0.0.0/8 <--> Localhost. LoopBack (lo) kendi makine içerisindeki ağ.

224.0.0.0/4 D <--> MultiCast. Her ip adresine teker teker göndermek yerine.
Herkese broadcast yapar gibi paketleri göndermek için ayrılmış ip bloğudur.

100.100.0.0/16 <--> Şu anda boşta olan ip bloğudur.

240.0.0.0/4 E <--> Bilimsel testler için ayrılmış ip bloğudur.

169.254.0.0/16 APIPA <--> DHCP serveri olmadığı zaman bu ip bloğundan rastgele
ip alır.

**[!] Yukarideki ip adresleri dışında private ip olarak başka bir bloğun
kullanılması durumunda internet üzerinde o ip bloğundaki sunuculara uzaktan
erişilmek istendiği zaman paket private ağ içerisinde gezeceği için kullanıcı
kendi ağı dışarısındaki sunucuya ulaşamayacaktır.**
