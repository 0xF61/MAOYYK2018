# OSINT

* **Seviye 1** : Otomatize araçlarla toplanıp sınır çizilir.
* **Seviye 2** : Kurum hakkında detaylı bilgi toplanır. (RED-TEAM)
* **Seviye 3** : Askeri, Devletler arası yapılır.

Bilgi Toplama:

* **Pasif** : Normal kullanıcıdan fark edilemeyecek şekilde bilgi toplamak
* **Aktif** : Hedefle doğrudan etkileşim halinde olunması (ServisTaraması,
  Fuzzing)

Sızma Testleri:

* **White** : Kaynak kod analizi
* **Gray** : Farklı seviyelerde kullanıcılar verilerek verilebilecek zarar
  hesaplanır
* **Black** : Sistem hakkında herhangi bir şey bilinmez

**Ağ Kapsam:** Sistemin ağ kapsamı ne kadar geniş hangi ip adreslerine **Otonom
Sistem Numarası** kullanarak hangi ip aralığına sahip olduğu araştırılır.

**Ağ Mimarisi:** Sunucular hakkında bilgiler toplanır.

**Aracıların Kullanılması:** İnternetten arama yapılabilir.

**Çalışanlar:** IT ekibi veya kaynak kod analizi yapılır.

**İfşalar:** İfşa olmuş daha önce hacklenilmiş bilgilerin araştırılması.

**Unutulmuş Dosya ve Dizin:** Sistemde hatırlanmayan bir sistemin veya bir
dizinin unutulmuş olması araştırılır.

**Subdomain/VirtualHost:** Sistemin sahip olduğu alt alan adları araştırılır.
Sunucuda başka sitelerin hizmetleride olabilir bunları araştırmak.

---

[Ters Domain Sorgusu yapılabilecek Siteler](http://www.irr.net/docs/list.html)

[Topoloji Çıkarabilecek Bir Site](https://www.robtex.com/)

[Mail Adresinden Domain Bulmak](https://viewdns.info/)


_**Subnet, AS** Otonom sistem numarası yakalanabilir._

``` $ whois -h whois.radb.net <ip> ```

``` $ whois -h whois.radb.net '!g<AS****>' ```

## Google Dork
* intext:
* inurl:
* site:
* filetype:
* inbody:
* intitle:
* \-
* " "
* numrange:

## Bing Dork
* ip:
* domain:

## Mail
* theharvester
* [haveibeenpwned](https://haveibeenpwned.com/)

## DNS
* fierce
    * Zone Transfer (axfr domain @ns)
* dig axfr @<site> zonetransfer.me
* [dnsdumpster](https://dnsdumpster.com/)

* [Shodan](https://www.shodan.io/)
    * hostname:
    * country:
    * os: 
    * net:

---

[WAPPALYZER](https://www.wappalyzer.com/)

[Censys](https://censys.io/)

[Archive](https://archive.org/)

[IPv4Info](http://ipv4info.com/)

[DNSTrails](https://securitytrails.com/dns-trails)

[Netcraft](https://www.netcraft.com)

[TinyEye](https://www.tineye.com)

