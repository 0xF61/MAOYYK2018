# ICMPv4

ICMP (Internet Control Message Protocol) Ağın sağlıklı bir şekilde çalıştığı,
ulaşılabilirliği, hız testi veya kontrol amaçlı kullanılan protokoldür.  Kendi
içerisinde bulunnan Type-Code olmak üzere değiştirilebilen variyantları
bulunmaktadır. Örnek olarak echo-reply 0-0 veya echo-request 8-0 verilebilir.

Daha fazla parametre [için](https://www.iana.org/assignments/icmp-parameters/icmp-parameters.xhtml)

ICMP type-code hakkında daha fazla örnek için;

* [Rfc792](https://tools.ietf.org/html/rfc792)

* [Rfc777](https://tools.ietf.org/html/rfc777)

* [Wireshark Örnek Paketler](https://wiki.wireshark.org/SampleCaptures)

**[!] OSI katmanı teorik olduğu ve kurulumu tam olarak yapılamayacağı için
TCP/IP modeli kullanılır**

---

## Scapy

Scapy içerisinde paket oluşturmak için kullanılan bir kütüphanedir.

```
$ scapy
>>> icmpbaslik = ICMP(type=14,code=0)
>>> ipbaslik = IP(src="192.168.42.82",dst="192.168.42.57")
>>> paket = ipbaslik/icmpbaslik
>>> send(paket)
.
Sent 1 packets.
```

**[!] Wireshark açıp ilgili network kartını dinleyerek gönderilen paketin
yanıtını görebiliriz.**

![icmp-timestamp](pic/icmp-timestamp.png)
